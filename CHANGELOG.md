# v1.2.1
- Added auto-updating.

# v1.2
- Fixed the layout in Chrome.

# v1.1

## Changes
- Updated the script to work with the new Twitch theme.

# v1.0

## Changes
- Initial release.
- Twitch chat is now replaced with [ChitChat](https://chitchat.ma.pe).
