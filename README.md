# Description
This userscript for Twitch allows you to load [ChitChat](https://chitchat.ma.pe/) as the default Chat for any stream.

# Installation
If you already use Tampermonkey click [this link](https://github.com/MarcGamesons/twitch-userscript-use-chitchat/raw/master/src/update/use-chitchat.user.js) to install the script.

1. Make sure that Tampermonkey for [Chrome](https://duckduckgo.com/?q=tampermonkey+chrome+webstore) or [Firefox](https://duckduckgo.com/?q=tampermonkey+firefox+addon) is installed.
2. Install the script by clicking [this link](https://github.com/MarcGamesons/twitch-userscript-use-chitchat/raw/master/src/update/use-chitchat.user.js).
